﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using static System.Diagnostics.Debug;

namespace ReSearcher
{
    /// <summary>Represents a position in file.</summary>
    public sealed class FilePosition
    {
        /// <summary>Creates a new instance of <see cref="FilePosition" />.</summary>
        /// <param name="line">The line number.</param>
        /// <param name="column">The column number on line.</param>
        internal FilePosition(Int32 line, Int32 column)
        {
            Assert(line > 0);
            Assert(column > 0);

            Line = line;
            Column = column;
        }

        /// <summary>The line number in file.</summary>
        public Int32 Line { get; }

        /// <summary>The column number on line.</summary>
        public Int32 Column { get; }
    }
}
