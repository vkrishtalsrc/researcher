﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace ReSearcher
{
    /// <summary>The strategy of index building.</summary>
    public enum IndexStrategy
    {
        /// <summary>
        /// The strategy determines just one fully tunable index for all files.
        /// This strategy is appropriate candidate if search required more often then updates
        /// Also the strategy is good candidate if long index rebuild is acceptable.
        /// The strategy isn't required high memory consumption.
        /// </summary>
        FullyTunableIndex,

        /// <summary>
        /// The strategy determines separated balanced index for each file.
        /// This strategy is appropriate candidate if updates required more often then search.
        /// The strategy is required more high memory consumption.
        /// </summary>
        BalancedMultiIndex
    }
}
