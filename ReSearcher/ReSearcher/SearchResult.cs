﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.Linq;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher
{
    /// <summary>Represents data of result of search.</summary>
    public sealed class SearchResult
    {
        /// <summary>Creates new instance of <see cref="SearchResult" />.</summary>
        /// <param name="fileName">The full file name.</param>
        /// <param name="positions">Positions in file.</param>
        internal SearchResult(String fileName, IEnumerable<FilePosition> positions)
        {
            Assert(!IsNullOrEmpty(fileName));
            Assert(positions != null);
            Assert(positions.Any());

            FileName = fileName;
            Positions = positions;
        }

        /// <summary>The full file name.</summary>
        public String FileName { get; }

        /// <summary>Positions in the file.</summary>
        public IEnumerable<FilePosition> Positions { get; }
    }
}
