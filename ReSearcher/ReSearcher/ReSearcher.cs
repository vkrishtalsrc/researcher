﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.IO;
using System.Threading.Tasks;
using ReSearcher.Exceptions;
using ReSearcher.Extensions;
using ReSearcher.Indexes;
using ReSearcher.Segmentation.Tokenizers;
using ReSearcher.Utils;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher
{
    /// <summary>Provides base facilities to indexing files and search text from indexed files.</summary>
    public sealed class ReSearcher : IResearcher, ISearcher, IDisposable
    {
        /// <summary>The index.</summary>
        private readonly IIndex _index;

        /// <summary>The index builder that must be determined by strategy type.</summary>
        private readonly IIndexBuilder _indexBuilder = new PlainTextIndexBuilder<WordTokenizer>();

        /// <summary>The watcher that watch changes on file system and updates indexes.</summary>
        private readonly IndexWatcher _watcher;

        /// <summary>The flag shows is instance disposed or not.</summary>
        private Boolean _disposed;

        /// <summary>Creates instance of <see cref="ReSearcher" />.</summary>
        public ReSearcher()
        {
            var index = new Indexes.BalancedIndexes.PrefixTree();
            _watcher = new BalancedIndexWatcher(index, _indexBuilder);
            _index = index;
        }

        /// <summary>Creates instance of <see cref="ReSearcher" />.</summary>
        /// <param name="indexType">The index type.</param>
        /// <param name="strategy">The index build strategy.</param>
        /// <exception cref="ArgumentOutOfRangeException">Type of index or index build strategy is unknown.</exception>
        /// <seealso cref="IndexType" />
        /// <seealso cref="strategy" />
        public ReSearcher(IndexType indexType, IndexStrategy strategy)
        {
            switch (strategy)
            {
                case IndexStrategy.BalancedMultiIndex:
                    var index = CreateBalancedIndex(indexType);
                    _watcher = new BalancedIndexWatcher(index, _indexBuilder);
                    _index = index;
                    break;

                case IndexStrategy.FullyTunableIndex:
                    _index = CreateFullyTunableIndex(indexType);
                    _watcher = new TunableIndexWatcher(_index, _indexBuilder);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(strategy), strategy,
                        "Unknown Index Build Strategy.");
            }
        }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
            DisposeResearcher();
            GC.SuppressFinalize(this);

            // it is good idea to ask GC to clear all because index could be heavy
            GC.Collect();
        }

        /// <summary>Checks file or directory is it added into the internal index to search.</summary>
        /// <param name="path">The relative or absolute path to the file or directory to check. This string is not case-sensitive.</param>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <returns>true if a target file or directory was added into the internal index; otherwise, false.</returns>
        public Boolean IsResearched(String path)
        {
            Assert(_watcher != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path),
                    "A path to file or directory can not be null or empty.");
            }

            return _watcher.Watched(path);
        }

        /// <summary>Adds a file into internal index to search.</summary>
        /// <param name="path">The relative or absolute path to the file. This string is not case-sensitive.</param>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="FileNotFoundException">The file was not found.</exception>
        /// <exception cref="AlreadyResearchedException">The file has already added into the index.</exception>
        public Task ResearchFileAsync(String path)
        {
            Assert(_index != null);
            Assert(_indexBuilder != null);
            Assert(_watcher != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path), "A path to file can not be null or empty.");
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The file to research was not found.", path);
            }

            if (IsResearched(path))
            {
                throw new AlreadyResearchedException(path);
            }


            _watcher.WatchFile(path);
            return _indexBuilder.IndexFileAsync(_index, path);
        }

        /// <summary>Adds files from a directory into internal index to search.</summary>
        /// <param name="path">The relative or absolute path to the directory to search. This string is not case-sensitive.</param>
        /// <param name="filter">
        ///     The filter to match against the names of files in directory.
        ///     This parameter can contain a combination of valid literal path and wildcard (* and ?) characters,
        ///     but it doesn't support regular expressions.
        /// </param>
        /// <param name="option">
        ///     One of the enumeration values that specifies
        ///     whether the search operation should include all subdirectories or only the current directory.
        /// </param>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="DirectoryNotFoundException">The directory was not found.</exception>
        /// <exception cref="AlreadyResearchedException">The directory has already added into the index.</exception>
        public Task ResearchDirectoryAsync(String path, String filter, SearchOption option)
        {
            Assert(_index != null);
            Assert(_indexBuilder != null);
            Assert(_watcher != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path),
                    "A path to directory can not be null or empty.");
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("The directory to research was not found.");
            }

            if (IsResearched(path))
            {
                throw new AlreadyResearchedException(path);
            }

            _watcher.WatchDirectory(path, filter, option);
            return _indexBuilder.IndexFilesAsync(_index, path, filter, option);
        }

        /// <summary>Adds a file into internal index to search.</summary>
        /// <param name="path">The relative or absolute path to the file. This string is not case-sensitive.</param>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="FileNotFoundException">The file was not found.</exception>
        /// <exception cref="AlreadyResearchedException">The file has already added into the index.</exception>
        public void ResearchFile(String path)
        {
            Assert(_index != null);
            Assert(_indexBuilder != null);
            Assert(_watcher != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path),
                    "A path to file can not be null or empty.");
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The file to research was not found.", path);
            }

            if (IsResearched(path))
            {
                throw new AlreadyResearchedException(path);
            }

            _watcher.WatchFile(path);
            _indexBuilder.IndexFile(_index, path);
        }

        /// <summary>Adds files from a directory into internal index to search.</summary>
        /// <param name="path">The relative or absolute path to the directory to search. This string is not case-sensitive.</param>
        /// <param name="filter">
        ///     The filter to match against the names of files in directory.
        ///     This parameter can contain a combination of valid literal path and wildcard (* and ?) characters,
        ///     but it doesn't support regular expressions.
        /// </param>
        /// <param name="option">
        ///     One of the enumeration values that specifies whether the search operation
        ///     should include all subdirectories or only the current directory.
        /// </param>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="DirectoryNotFoundException">The directory was not found.</exception>
        /// <exception cref="AlreadyResearchedException">The directory has already added into the index.</exception>
        public void ResearchDirectory(String path, String filter, SearchOption option)
        {
            Assert(_index != null);
            Assert(_indexBuilder != null);
            Assert(_watcher != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path), "A path to directory can not be null or empty.");
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException("The directory to research was not found.");
            }

            if (IsResearched(path))
            {
                throw new AlreadyResearchedException(path);
            }

            _watcher.WatchDirectory(path, filter, option);
            _indexBuilder.IndexFiles(_index, path, filter, option);
        }

        /// <summary> Starts search of text.</summary>
        /// <param name="text">The text to search.</param>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="text" /> is null or empty.</exception>
        /// <exception cref="InvalidOperationException">The internal index is empty - nothing researched.</exception>
        /// <exception cref="ObjectDisposedException">The instance has already disposed.</exception>
        /// <seealso cref="IObservable{T}" />
        /// <seealso cref="SearchResult" />
        /// <returns>Push-based notification about result.</returns>
        public IObservable<SearchResult> Search(String text)
        {
            Assert(_index != null);

            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(ReSearcher));
            }

            if (IsNullOrWhiteSpace(text))
            {
                throw new ArgumentNullOrEmptyException(nameof(text),
                    "A text to search can not be null or empty.");
            }

            return _index
                   .Search(text)
                   .WhereNotNull();
        }


        /// <summary>
        ///     Finalizes instance of <see cref="ReSearcher" />.
        ///     Frees resources if somebody forgot to call Dispose method.
        /// </summary>
        ~ReSearcher()
        {
            DisposeResearcher();
        }

        /// <summary>Creates new instance of balanced multi-index.</summary>
        /// <param name="indexType">The index type.</param>
        /// <returns>The index.</returns>
        private IBalancedIndex CreateBalancedIndex(IndexType indexType)
        {
            switch (indexType)
            {
                case IndexType.PrefixTree:
                    return new Indexes.BalancedIndexes.PrefixTree();

                case IndexType.RadixTree:
                    // TODO: Radix tree is not implemented yet. It will throw NotImplementedException.
                    return new Indexes.BalancedIndexes.RadixTree();

                case IndexType.HashTable:
                    return new Indexes.BalancedIndexes.HashTable();

                default:
                    throw new ArgumentOutOfRangeException(nameof(indexType), indexType, "Unknown index type.");
            }
        }

        /// <summary>Creates new instance of fully tunable single-index.</summary>
        /// <param name="indexType">The index type.</param>
        /// <returns>The index.</returns>
        private IIndex CreateFullyTunableIndex(IndexType indexType)
        {
            switch (indexType)
            {
                case IndexType.PrefixTree:
                    return new Indexes.TunableIndexes.PrefixTree();

                case IndexType.RadixTree:
                    // TODO: Radix tree is not implemented yet. It will throw NotImplementedException.
                    return new Indexes.TunableIndexes.RadixTree();

                case IndexType.HashTable:
                    return new Indexes.TunableIndexes.HashTable();

                default:
                    throw new ArgumentOutOfRangeException(nameof(indexType), indexType, "Unknown index type.");
            }
        }

        /// <summary>Disposes ReSearcher instance.</summary>
        private void DisposeResearcher()
        {
            _disposed = true;
            _watcher?.Dispose();
            _index.Clear();
        }
    }
}
