﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using static System.Diagnostics.Debug;

namespace ReSearcher.Exceptions
{
    /// <summary>The exception that signals about unexpected null or empty argument.</summary>
    public sealed class ArgumentNullOrEmptyException : ArgumentException
    {
        /// <summary>The default error message.</summary>
        private const String DEFAULT_MESSAGE = "Parameter cannot be null or empty.";

        /// <summary>Creates instance of <see cref="ArgumentNullOrEmptyException " />.</summary>
        public ArgumentNullOrEmptyException() : base(DEFAULT_MESSAGE)
        {
        }

        /// <summary>Creates instance of <see cref="ArgumentNullOrEmptyException " />.</summary>
        /// <param name="paramName">The parameter name.</param>
        public ArgumentNullOrEmptyException(String paramName) : base(DEFAULT_MESSAGE, paramName)
        {
            Assert(!String.IsNullOrWhiteSpace(paramName));
        }

        /// <summary>Creates instance of <see cref="ArgumentNullOrEmptyException " />.</summary>
        /// <param name="paramName">The parameter name.</param>
        /// <param name="message">The error message.</param>
        public ArgumentNullOrEmptyException(String paramName, String message) : base(message, paramName)
        {
            Assert(!String.IsNullOrWhiteSpace(paramName));
            Assert(!String.IsNullOrWhiteSpace(message));
        }
    }
}
