﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using ReSearcher.Exceptions;

namespace ReSearcher
{
    /// <summary>Provides base facilities to search of text.</summary>
    public interface ISearcher
    {
        /// <summary> Starts search of text.</summary>
        /// <param name="text">The text to search.</param>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="text" /> is null or empty.</exception>
        /// <exception cref="InvalidOperationException">The internal index is empty - nothing researched.</exception>
        /// <seealso cref="IObservable{T}" />
        /// <seealso cref="SearchResult" />
        /// <returns>Push-based notification about result.</returns>
        IObservable<SearchResult> Search(String text);
    }
}
