﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

namespace ReSearcher
{
    /// <summary>
    ///     The index type.
    ///     Different indexes have got different properties.
    ///     Choose specific index type for specific case.
    /// </summary>
    public enum IndexType
    {
        /// <summary>
        ///     Kind of search tree—an ordered tree data structure that is used to store a dynamic set or associative array.
        ///     Unlike a binary search tree, no node in the tree stores the key associated with that node;
        ///     instead, its position in the tree defines the key with which it is associated.
        ///     All the descendants of a node have a common prefix of the string associated with that node,
        ///     and the root is associated with the empty string.
        ///     Values are not necessarily associated with every node.
        ///     Rather, values tend only to be associated with leaves, and with some inner nodes that correspond to keys of
        ///     interest.
        ///     This index type is appropriate candidate to quick search if it is requred to index a lot of data without economy of
        ///     memory.
        /// </summary>
        PrefixTree,

        /// <summary>
        ///     The space-optimized prefix tree in which each node that is the only child is merged with its parent.
        ///     This index type is appropriate candidate to quick search if it is requred to index a lot of data with economy of
        ///     memory
        ///     and longer index rebuild is acceptable.
        /// </summary>
        RadixTree,

        /// <summary>
        ///     The associative array that can map keys to values.
        ///     A hash table uses a hash function to compute an index into an array of buckets or slots, from which the desired
        ///     value can be found.
        ///     It means that some collisions are possible. Collisions can degrade perfomance at whole.
        ///     This index type is appropriate candidate to quick search if it isn't requred to index a huge data.
        /// </summary>
        HashTable
    }
}
