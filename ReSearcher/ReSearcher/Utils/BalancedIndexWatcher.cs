﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using ReSearcher.Indexes;
using static System.Diagnostics.Debug;
using static System.IO.File;

namespace ReSearcher.Utils
{
    /// <summary>The index watcher that helps to update Balanced indexes.</summary>
    internal sealed class BalancedIndexWatcher : IndexWatcher
    {
        /// <summary>The index builder.</summary>
        private readonly IIndexBuilder _builder;

        /// <summary>The balanced index.</summary>
        private readonly IBalancedIndex _index;

        /// <summary>Creates a new instance of <see cref="BalancedIndexWatcher" />.</summary>
        /// <param name="index">The index.</param>
        /// <param name="builder">THe index builder.</param>
        public BalancedIndexWatcher(IBalancedIndex index, IIndexBuilder builder)
        {
            Assert(index != null);
            Assert(builder != null);

            _index = index;
            _builder = builder;
        }

        /// <summary>Updates index after something changed.</summary>
        protected override void WatcherOnChanged(Object o, WatcherChangedArgs args)
        {
            Assert(args != null);
            Assert(_index != null);
            Assert(_builder != null);

            _index.ClearFor(args.Path);

            if (Exists(args.Path))
            {
                _builder.IndexFile(_index, args.Path);
            }
        }
    }
}
