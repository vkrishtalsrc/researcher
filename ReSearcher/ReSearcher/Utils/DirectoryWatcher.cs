﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ReSearcher.Exceptions;
using static System.Diagnostics.Debug;

namespace ReSearcher.Utils
{
    /// <summary>Listens to the file system change notifications and raises events when a directory changes.</summary>
    internal sealed class DirectoryWatcher : IWatcher
    {
        /// <summary>The common watcher that need to handling events like create\rename\delete.</summary>
        private readonly FileSystemWatcher _directoryWatcher;

        /// <summary>The file watchers for files in this directory.</summary>
        private readonly Dictionary<String, FileWatcher> _fileWatchers = new Dictionary<String, FileWatcher>();

        /// <summary>Creates a new instance of<see cref="DirectoryWatcher" />.</summary>
        /// <param name="path">The directory path.</param>
        /// <param name="filter">The filter string used to determine what files are monitored in a directory.</param>
        /// <param name="option">
        ///     One of the enumeration values that specifies whether the search operation should include all
        ///     subdirectories or only the current directory.
        /// </param>
        /// <exception cref="ArgumentNullOrEmptyException">The path or filter is null or empty.</exception>
        public DirectoryWatcher(String path, String filter, SearchOption option)
        {
            if (String.IsNullOrEmpty(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (String.IsNullOrEmpty(filter))
            {
                throw new ArgumentNullOrEmptyException(nameof(filter));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException();
            }

            WatchPath = path;
            Filter = filter;
            Option = option;

            _directoryWatcher = new FileSystemWatcher();
            _directoryWatcher.Path = path;
            _directoryWatcher.Filter = filter;
            _directoryWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite |
                                             NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.Size;
            ;
            _directoryWatcher.Created += DirectoryWatcherOnCreated;
            _directoryWatcher.Deleted += DirectoryWatcherOnDeleted;
            _directoryWatcher.Renamed += DirectoryWatcherOnRenamed;
            _directoryWatcher.EnableRaisingEvents = true;

            var files = Directory.GetFiles(path, filter, option);

            foreach (var file in files)
            {
                var watcher = new FileWatcher(file);
                watcher.Changed += WatcherOnChanged;
                _fileWatchers.Add(file, watcher);
            }
        }

        /// <summary>The filter string used to determine what files are monitored in a directory.</summary>
        public String Filter { get; }

        /// <summary>
        ///     One of the enumeration values that specifies whether the search operation should include all
        ///     subdirectories or only the current directory.
        /// </summary>
        public SearchOption Option { get; }

        /// <summary>
        ///     The path to watchable directory .
        /// </summary>
        public String WatchPath { get; }

        /// <summary>Returns true if item by specified path has already is watching.</summary>
        /// <param name="path">The path to file or directory.</param>
        /// <returns>true if item by specified path has already watched.</returns>
        public Boolean Watched(String path)
        {
            Assert(WatchPath != null);
            Assert(_fileWatchers != null);

            return WatchPath.Equals(path) || _fileWatchers.Values.Any(watcher => watcher.Watched(path));
        }

        /// <summary>Raised if file or directory changed.</summary>
        public event EventHandler<WatcherChangedArgs> Changed;

        /// <summary>Frees resources.</summary>
        public void Dispose()
        {
            DisposeWatcher();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Finalizes instance of <see cref="DirectoryWatcher" />.
        ///     Frees resources if somebody forgot to call Dispose method.
        /// </summary>
        ~DirectoryWatcher()
        {
            DisposeWatcher();
        }

        /// <summary>Frees resources.</summary>
        private void DisposeWatcher()
        {
            Assert(_fileWatchers != null);

            foreach (var watcher in _fileWatchers.Values)
            {
                watcher.Dispose();
            }

            _directoryWatcher?.Dispose();
        }

        /// <summary>The event handler.</summary>
        private void WatcherOnChanged(Object sender, WatcherChangedArgs args)
        {
            Changed?.Invoke(sender, new WatcherChangedArgs(args.Path));
        }

        /// <summary>The event handler.</summary>
        private void DirectoryWatcherOnRenamed(Object sender, RenamedEventArgs args)
        {
            Assert(args != null);

            UnwatchFile(args.OldFullPath);
            WatchFile(args.FullPath);
        }

        /// <summary>The event handler.</summary>
        private void DirectoryWatcherOnDeleted(Object sender, FileSystemEventArgs args)
        {
            Assert(args != null);
            Assert(_fileWatchers != null);

            if (_fileWatchers.ContainsKey(args.FullPath))
            {
                UnwatchFile(args.FullPath);
            }

            Changed?.Invoke(sender, new WatcherChangedArgs(args.FullPath));
        }

        /// <summary>The event handler.</summary>
        private void DirectoryWatcherOnCreated(Object sender, FileSystemEventArgs args)
        {
            Assert(args != null);
            Assert(_fileWatchers != null);

            if (!_fileWatchers.ContainsKey(args.FullPath))
            {
                WatchFile(args.FullPath);
            }

            Changed?.Invoke(sender, new WatcherChangedArgs(args.FullPath));
        }

        /// <summary>The event handler.</summary>
        private void WatchFile(String path)
        {
            Assert(!String.IsNullOrWhiteSpace(path));
            Assert(File.Exists(path));
            Assert(_fileWatchers != null);
            Assert(!_fileWatchers.ContainsKey(path));

            var watcher = new FileWatcher(path);
            watcher.Changed += WatcherOnChanged;
            _fileWatchers.Add(path, watcher);
        }

        /// <summary>The event handler.</summary>
        private void UnwatchFile(String path)
        {
            Assert(!String.IsNullOrWhiteSpace(path));
            Assert(_fileWatchers != null);
            Assert(_fileWatchers.ContainsKey(path));

            _fileWatchers[path].Dispose();
            _fileWatchers.Remove(path);
        }
    }
}
