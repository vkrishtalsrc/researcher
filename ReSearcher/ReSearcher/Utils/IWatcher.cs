﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;

namespace ReSearcher.Utils
{
    /// <summary>Provides base interface for file and directory watcher.</summary>
    internal interface IWatcher : IDisposable
    {
        /// <summary>Path to watchable file or directory.</summary>
        String WatchPath { get; }

        /// <summary>Raised if file or directory changed.</summary>
        event EventHandler<WatcherChangedArgs> Changed;

        /// <summary>Returns true if item by specified path has already is watching.</summary>
        /// <param name="path">The path to file or directory.</param>
        /// <returns>true if item by specified path has already watched.</returns>
        Boolean Watched(String path);
    }
}
