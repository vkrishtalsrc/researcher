﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.IO;
using ReSearcher.Exceptions;
using static System.Diagnostics.Debug;
using static System.IO.File;
using static System.String;

namespace ReSearcher.Utils
{
    /// <summary>Listens to the file system change notifications and raises events when a directory changes.</summary>
    internal sealed class FileWatcher : IWatcher
    {
        /// <summary>The internal file watcher.</summary>
        private readonly FileSystemWatcher _watcher;

        /// <summary>Creates a new instance of <see cref="FileWatcher" />.</summary>
        /// <param name="path">The full file name.</param>
        /// <exception cref="FileNotFoundException">The file to watch was not found.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The path is null or empty.</exception>
        public FileWatcher(String path)
        {
            if (IsNullOrEmpty(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (!Exists(path))
            {
                throw new FileNotFoundException("The file to watch was not found.", path);
            }

            WatchPath = path;

            _watcher = new FileSystemWatcher();
            _watcher.Path = Path.GetDirectoryName(path);
            _watcher.Filter = Path.GetFileName(path);
            _watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite |
                                    NotifyFilters.FileName | NotifyFilters.DirectoryName;

            _watcher.Changed += WatcherOnChanged;
            _watcher.EnableRaisingEvents = true;
        }

        /// <summary>The file path.</summary>
        public String WatchPath { get; }

        /// <summary>Returns true if item by specified path has already is watching.</summary>
        /// <param name="path">The path to file or directory.</param>
        /// <returns>true if item by specified path has already watched.</returns>
        public Boolean Watched(String path)
        {
            Assert(WatchPath != null);
            return WatchPath.Equals(path);
        }

        /// <summary>Occurs when a file in the specified Path is changed.</summary>
        public event EventHandler<WatcherChangedArgs> Changed;

        /// <summary>Frees resources.</summary>
        public void Dispose()
        {
            DisposeWatcher();
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Finalizes instance of <see cref="FileWatcher" />.
        ///     Frees resources if somebody forgot to call Dispose method.
        /// </summary>
        ~FileWatcher()
        {
            DisposeWatcher();
        }

        /// <summary>Frees resources.</summary>
        private void DisposeWatcher()
        {
            if (_watcher != null)
            {
                _watcher.Changed -= WatcherOnChanged;
                _watcher.EnableRaisingEvents = false;
                _watcher.Dispose();
            }
        }

        /// <summary>The file changed event handler.</summary>
        private void WatcherOnChanged(Object sender, FileSystemEventArgs e)
        {
            Assert(!IsNullOrEmpty(WatchPath));

            Changed?.Invoke(sender, new WatcherChangedArgs(WatchPath));
        }
    }
}
