﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Diagnostics.Debug;

namespace ReSearcher.Utils
{
    /// <summary>The util helper class that helps determine when we need update index.</summary>
    internal abstract class IndexWatcher : IDisposable
    {
        /// <summary>Internal file system watchers.</summary>
        private readonly ConcurrentDictionary<String, IWatcher>
            _watchers = new ConcurrentDictionary<String, IWatcher>();

        /// <summary>Internal file system watchers.</summary>
        protected IEnumerable<IWatcher> Watchers => _watchers.Values;

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose()
        {
            foreach (var watcher in _watchers)
            {
                watcher.Value?.Dispose();
            }
        }

        /// <summary>Returns true if item by specified path has already is watching.</summary>
        /// <param name="path">The path to file or directory.</param>
        /// <returns>true if item by specified path has already watched.</returns>
        public Boolean Watched(String path)
        {
            Assert(Watchers != null);
            return Watchers.Any(watcher => watcher.Watched(path));
        }

        /// <summary>Starts watching.</summary>
        /// <param name="file">The file path.</param>
        public void WatchFile(String file)
        {
            if (!_watchers.ContainsKey(file))
            {
                var watcher = new FileWatcher(file);
                watcher.Changed += WatcherOnChanged;
                _watchers.TryAdd(file, watcher);
            }
        }

        /// <param name="path">The directory path.</param>
        /// <param name="filter">The filter string used to determine what files are monitored in a directory.</param>
        /// <param name="option">
        ///     One of the enumeration values that specifies whether the search operation should include all
        ///     subdirectories or only the current directory.
        /// </param>
        public void WatchDirectory(String path, String filter, SearchOption option)
        {
            if (!_watchers.ContainsKey(path))
            {
                var watcher = new DirectoryWatcher(path, filter, option);
                watcher.Changed += WatcherOnChanged;
                _watchers.TryAdd(path, watcher);
            }
        }

        /// <summary>The event handler.</summary>
        protected abstract void WatcherOnChanged(Object o, WatcherChangedArgs args);
    }
}
