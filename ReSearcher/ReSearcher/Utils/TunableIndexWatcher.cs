﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.IO;
using ReSearcher.Indexes;
using static System.Diagnostics.Debug;

namespace ReSearcher.Utils
{
    /// <summary>The index watcher that helps to update Balanced indexes.</summary>
    internal sealed class TunableIndexWatcher : IndexWatcher
    {
        /// <summary>The index builder.</summary>
        private readonly IIndexBuilder _builder;

        /// <summary>The fully tunable index.</summary>
        private readonly IIndex _index;

        /// <summary>Creates a new instance of <see cref="TunableIndexWatcher" />.</summary>
        /// <param name="index">The fully tunable index.</param>
        /// <param name="builder">The index builder.</param>
        public TunableIndexWatcher(IIndex index, IIndexBuilder builder)
        {
            Assert(index != null);
            Assert(builder != null);

            _index = index;
            _builder = builder;
        }

        /// <summary>Updates index after something changed.</summary>
        protected override void WatcherOnChanged(Object o, WatcherChangedArgs args)
        {
            Assert(_index != null);
            Assert(_builder != null);

            _index.Clear();

            foreach (var watcher in Watchers)
            {
                switch (watcher)
                {
                    case FileWatcher fileWatcher when File.Exists(fileWatcher.WatchPath):
                        _builder.IndexFile(_index, fileWatcher.WatchPath);
                        break;

                    case DirectoryWatcher dirWatcher when Directory.Exists(dirWatcher.WatchPath):
                        _builder.IndexFiles(_index, dirWatcher.WatchPath, dirWatcher.Filter, dirWatcher.Option);
                        break;

                    // do nothing by default
                }
            }
        }
    }
}
