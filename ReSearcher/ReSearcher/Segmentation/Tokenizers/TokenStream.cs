﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher.Segmentation.Tokenizers
{
    /// <summary>A tokenizer receives a stream of characters, breaks it up into individual tokens (usually individual words).</summary>
    internal class TokenStream<T> : ITokenStream<T> where T : IEquatable<T>
    {
        /// <summary>The stream reader to read source text.</summary>
        private readonly StreamReader _streamReader;

        /// <summary>Matchers to match text.</summary>
        private readonly IEnumerable<ITokenMatcher<T>> _tokenMatchers;

        /// <summary>The current read line from <see cref="_streamReader" />.</summary>
        private String _currentLine;

        /// <summary>The index that referred to symbol from <see cref="_currentLine" />.</summary>
        private Int32 _index;

        /// <summary>The file line number of <see cref="_currentLine" />.</summary>
        private Int32 _lineNumber;

        /// <summary>Creates new instance of <see cref="TokenStream{T}" />.</summary>
        /// <param name="stream">The stream to read source text. Tokenizer becomes owner of this stream and will dispose it.</param>
        /// <param name="tokenMatchers">Token matchers to match token from source text.</param>
        /// <exception cref="ArgumentNullException">The stream or tokenMatchers are null.</exception>
        /// <exception cref="ArgumentException">The stream doesn't support reading or tokenMatchers are empty.</exception>
        public TokenStream(Stream stream, IEnumerable<ITokenMatcher<T>> tokenMatchers)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanRead)
            {
                throw new ArgumentException("Can read nothing from the input stream.",
                    nameof(stream));
            }

            if (tokenMatchers == null)
            {
                throw new ArgumentNullException(nameof(tokenMatchers));
            }

            if (!tokenMatchers.Any())
            {
                throw new ArgumentException("Token matchers are required to tokenization.", nameof(tokenMatchers));
            }

            _streamReader = new StreamReader(stream);
            _tokenMatchers = tokenMatchers;
        }

        /// <summary>Frees resources.</summary>
        public void Dispose()
        {
            _streamReader?.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <summary>Returns a next available segment.</summary>
        /// <returns>The segment if it is exists; otherwise, null.</returns>
        public async Task<Segment> NextSegmentAsync()
        {
            var token = await NextTokenAsync();
            return token != null ? new Segment(token.Value, token.Position.Line, token.Position.Column) : null;
        }

        /// <summary>Returns a next available segment.</summary>
        /// <returns>The segment if it is exists; otherwise, null.</returns>
        public Segment NextSegment()
        {
            var token = NextToken();
            return token != null ? new Segment(token.Value, token.Position.Line, token.Position.Column) : null;
        }

        /// <summary>Returns a next available token.</summary>
        /// <returns>The token if token is exists; otherwise, null.</returns>
        public async Task<Token<T>> NextTokenAsync()
        {
            if (!CanNext())
            {
                // can not return token because file and current read line is over
                return null;
            }

            if (IsEmptyLine())
            {
                // current read line is over - read next
                await ReadNextLineAsync();
            }

            if (IsEmptyLine())
            {
                // next line is empty too
                return null;
            }

            // use matcher to try to get token by current position
            var token = MatchToken();

            if (token == null)
            {
                // matchers didn't found something on current position
                // it means we need increase counter and try again
                _index++;
                return await NextTokenAsync();
            }

            return token;
        }

        /// <summary>Returns a next available segment.</summary>
        /// <returns>The token if token is exists; otherwise, null.</returns>
        public Token<T> NextToken()
        {
            if (!CanNext())
            {
                // can not return token because file and read line is over
                return null;
            }

            if (IsEmptyLine())
            {
                // current read line is over - read next
                ReadNextLine();
            }

            if (IsEmptyLine())
            {
                // next line is empty too
                return null;
            }

            // use matcher to try to get token by current position
            var token = MatchToken();

            if (token == null)
            {
                // matchers didn't found something on current position
                // it means we need increase counters and try again
                _index++;

                return NextToken();
            }

            return token;
        }

        /// <summary>
        ///     Finalizes instance of <see cref="TokenStream{T}" />.
        ///     Frees resources if somebody forgot to call Dispose method.
        /// </summary>
        ~TokenStream()
        {
            _streamReader?.Dispose();
        }

        /// <summary>Matches a token on current line by current position.</summary>
        /// <returns>The matched token.</returns>
        private Token<T> MatchToken()
        {
            Assert(_tokenMatchers != null);
            Assert(!IsNullOrEmpty(_currentLine));

            foreach (var matcher in _tokenMatchers)
            {
                var matched = matcher.Match(_currentLine, _index);

                if (matched > 0)
                {
                    var term = _currentLine.Substring(_index, matched);
                    var token = new Token<T>(term, matcher.TokenType, _lineNumber, _index + 1);
                    _index += matched;
                    return token;
                }
            }

            return null;
        }

        /// <summary>Reads next line from stream.</summary>
        private void ReadNextLine()
        {
            // drop counter
            _index = 0;

            do
            {
                // skip empty lines
                _currentLine = _streamReader.ReadLine();
                _lineNumber++;
            } while (!_streamReader.EndOfStream && IsNullOrWhiteSpace(_currentLine));
        }

        /// <summary>Reads next line from stream.</summary>
        private async Task ReadNextLineAsync()
        {
            // drop counter
            _index = 0;

            do
            {
                // skip empty lines
                _currentLine = await _streamReader.ReadLineAsync();
                _lineNumber++;
            } while (!_streamReader.EndOfStream && IsNullOrWhiteSpace(_currentLine));
        }

        /// <summary>Checks current line to empty.</summary>
        private Boolean IsEmptyLine() => IsNullOrEmpty(_currentLine) || _index >= _currentLine.Length;

        /// <summary>Checks tokenizer to available to continue getting tokens.</summary>
        private Boolean CanNext()
        {
            Assert(_streamReader != null);
            return !IsEmptyLine() || !_streamReader.EndOfStream;
        }
    }
}
