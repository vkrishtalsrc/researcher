﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using static System.Diagnostics.Debug;

namespace ReSearcher.Segmentation.Tokenizers.Matchers
{
    /// <summary>The abstract matchers to match token by char type.</summary>
    internal abstract class CharMatcher : ITokenMatcher<String>
    {
        /// <summary>Returns token type specified for the current token matcher.</summary>
        public abstract String TokenType { get; }

        /// <summary>Returns the number of characters that matched.</summary>
        /// <param name="text">The text to be matched.</param>
        /// <param name="begin">The index of first symbol that can be checked.</param>
        /// <returns>The number of characters that matched.</returns>
        public Int32 Match(String text, Int32 begin)
        {
            Assert(!String.IsNullOrEmpty(text));
            Assert(begin >= 0);
            Assert(begin < text.Length);

            var count = 0;
            while (text.Length > begin && Match(text[begin]))
            {
                count++;
                begin++;
            }

            return count;
        }

        /// <summary>Matches concrete symbol from source string.</summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        protected abstract Boolean Match(Char symbol);
    }
}
