﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ReSearcher.Exceptions;

namespace ReSearcher.Segmentation.Tokenizers.Matchers
{
    /// <summary>The matchers to match substring from source string by regex.</summary>
    internal class RegexTokenMatcher : ITokenMatcher<String>
    {
        /// <summary>The regex to match substring.</summary>
        private readonly Regex _regex;

        /// <summary>Creates a new instance of <see cref="RegexTokenMatcher" />.</summary>
        /// <param name="regex">The regex value.</param>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="regex" /> is null or empty.</exception>
        public RegexTokenMatcher(String regex)
        {
            if (String.IsNullOrEmpty(regex))
            {
                throw new ArgumentNullOrEmptyException(nameof(regex));
            }

            _regex = new Regex(regex);
        }

        /// <summary>Returns token type specified for the current token matcher.</summary>
        public String TokenType => _regex.ToString();

        /// <summary>Returns the number of characters that matched.</summary>
        /// <param name="text">The text to be matched.</param>
        /// <param name="begin">The index of first symbol that can be checked.</param>
        /// <returns>The number of characters that matched.</returns>
        public Int32 Match(String text, Int32 begin)
        {
            var size = 0;
            var match = _regex.Match(text, begin);
            while (match.Success)
            {
                size += match.Length;
                match = _regex.Match(text, begin + size);
            }

            return size;
        }

        /// <summary>
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public (Int32 begin, Int32 size) Match(String text)
        {
            Debug.Assert(_regex != null);

            var match = _regex.Match(text);
            return match.Success ? (match.Index, match.Length) : (0, 0);
        }
    }
}
