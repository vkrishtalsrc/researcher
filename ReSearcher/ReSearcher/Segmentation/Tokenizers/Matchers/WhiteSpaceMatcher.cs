﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;

namespace ReSearcher.Segmentation.Tokenizers.Matchers
{
    /// <summary>The matchers to match whitespaces from source string.</summary>
    internal sealed class WhiteSpaceMatcher : CharMatcher
    {
        /// <summary>Returns token type specified for the current token matcher.</summary>
        public override String TokenType => "WhiteSpace";

        /// <summary>Matches concrete symbol from source string.</summary>
        /// <param name="symbol"></param>
        /// <returns></returns>
        protected override Boolean Match(Char symbol) => Char.IsWhiteSpace(symbol);
    }
}
