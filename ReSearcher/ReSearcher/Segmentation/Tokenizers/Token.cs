﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Diagnostics;
using static System.Diagnostics.Debug;

namespace ReSearcher.Segmentation.Tokenizers
{
    /// <summary>Represents a text token from a file.</summary>
    /// <typeparam name="T">The type of token.</typeparam>
    [DebuggerDisplay("{" + nameof(Value) + "}")]
    internal sealed class Token<T> : Segment where T : IEquatable<T>
    {
        /// <summary>Creates a new instance of <see cref="Segment" />.</summary>
        /// <param name="value">The segment value. Can be empty but can't be null.</param>
        /// <param name="tokenType"></param>
        /// <param name="lineNumber">The line number of the segment if file.</param>
        /// <param name="positionOnLine">The position of this segment on the line.</param>
        public Token(String value, T tokenType, Int32 lineNumber, Int32 positionOnLine)
            : base(value, lineNumber, positionOnLine)
        {
            Assert(!String.IsNullOrEmpty(value));
            Assert(lineNumber > 0);
            Assert(positionOnLine > 0);

            Type = tokenType;
        }

        /// <summary>The token type.</summary>
        public T Type { get; }
    }
}
