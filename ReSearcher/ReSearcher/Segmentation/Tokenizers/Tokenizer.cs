﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using ReSearcher.Exceptions;
using static System.Diagnostics.Debug;
using static System.IO.File;
using static System.String;

namespace ReSearcher.Segmentation.Tokenizers
{
    /// <summary>Represents base tokenizer.</summary>
    /// <typeparam name="T"></typeparam>
    internal class Tokenizer<T> : ITokenizer<T> where T : IEquatable<T>
    {
        /// <summary>Token matchers.</summary>
        private readonly IEnumerable<ITokenMatcher<T>> _matchers;

        /// <summary>Creates a new instance of <see cref="Tokenizer{T}" />.</summary>
        /// <param name="matchers">Matchers to match strings.</param>
        public Tokenizer(IEnumerable<ITokenMatcher<T>> matchers)
        {
            Assert(matchers != null);

            _matchers = matchers;
        }

        /// <summary>Starts file tokenization.</summary>
        /// <param name="name">The file name.</param>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="name" /> is null or empty.</exception>
        /// <exception cref="FileNotFoundException">The file to segment was not found.</exception>
        /// <returns>The token stream.</returns>
        public ITokenStream<T> StartTokenization(String name)
        {
            if (IsNullOrEmpty(name))
            {
                throw new ArgumentNullOrEmptyException(nameof(name));
            }

            if (!Exists(name))
            {
                throw new FileNotFoundException("The file to segment was not found.", name);
            }


            var stream = OpenRead(name);
            return new TokenStream<T>(stream, _matchers);
        }

        /// <summary>Starts file segmentation.</summary>
        /// <param name="name">The file name.</param>
        /// <returns>The file segment scanner.</returns>
        public ISegmentStream StartSegmentation(String name) => StartTokenization(name);
    }
}
