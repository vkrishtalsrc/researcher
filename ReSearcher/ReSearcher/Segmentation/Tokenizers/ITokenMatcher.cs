﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;

namespace ReSearcher.Segmentation.Tokenizers
{
    /// <summary>Provides the base interface for the token matchers.</summary>
    /// <typeparam name="T">The token type.</typeparam>
    internal interface ITokenMatcher<out T> where T : IEquatable<T>
    {
        /// <summary>Returns token type specified for the current token matcher.</summary>
        T TokenType { get; }

        /// <summary>Returns the number of characters that matched.</summary>
        /// <param name="text">The text to be matched.</param>
        /// <param name="begin">The index of first symbol that can be checked.</param>
        /// <returns>The number of characters that matched.</returns>
        Int32 Match(String text, Int32 begin = 0);
    }
}
