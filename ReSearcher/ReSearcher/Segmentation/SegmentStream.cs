﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher.Segmentation
{
    /// <summary>Represents stream of file segments.</summary>
    internal class SegmentStream : ISegmentStream
    {
        /// <summary>The delimiters to split text from read stream.</summary>
        private readonly Char[] _delimiters;

        /// <summary>The read from file segments.</summary>
        private readonly Queue<Segment> _segments = new Queue<Segment>();

        /// <summary>The stream reader.</summary>
        private readonly StreamReader _stream;

        /// <summary>The number of lst read from stream line.</summary>
        private Int32 _lineNumber;

        /// <summary>Creates a new instance of <see cref="SegmentStream" />.</summary>
        /// <param name="stream">The stream to read text and segment it.</param>
        /// <param name="delimiters">The delimiters that must be used to split text.</param>
        /// <exception cref="ArgumentNullException">The stream or delimiters are null.</exception>
        /// <exception cref="ArgumentException">The stream doesn't support reading or delimiters are empty.</exception>
        public SegmentStream(Stream stream, IEnumerable<Char> delimiters)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanRead)
            {
                throw new ArgumentException("Can read nothing from the input stream.",
                    nameof(stream));
            }

            if (_delimiters == null)
            {
                throw new ArgumentNullException(nameof(_delimiters));
            }

            _stream = new StreamReader(stream);
            _delimiters = delimiters.ToArray();
        }

        /// <summary>Frees resources.</summary>
        public void Dispose()
        {
            _stream?.Dispose();
            GC.SuppressFinalize(this);
        }

        /// <summary>Returns a next available segment.</summary>
        /// <returns>The segment if it is exists; otherwise, null.</returns>
        public async Task<Segment> NextSegmentAsync()
        {
            Assert(_stream != null);
            Assert(_segments != null);

            while (!_stream.EndOfStream && !_segments.Any())
            {
                await SegmentNextLineAsync();
            }

            return _segments.Any() ? _segments.Dequeue() : null;
        }

        /// <summary>Returns a next available segment.</summary>
        /// <returns>The segment if it is exists; otherwise, null.</returns>
        public Segment NextSegment()
        {
            Assert(_stream != null);
            Assert(_segments != null);

            while (!_stream.EndOfStream && !_segments.Any())
            {
                SegmentNextLine();
            }

            return _segments.Any() ? _segments.Dequeue() : null;
        }

        /// <summary>
        ///     Finalizes instance of <see cref="SegmentStream" />.
        ///     Frees resources if somebody forgot to call Dispose method.
        /// </summary>
        ~SegmentStream()
        {
            _stream?.Dispose();
        }

        /// <summary>Segments next line from stream.</summary>
        private void SegmentNextLine()
        {
            Assert(_stream != null);

            var line = ReadNextLine();

            if (!IsNullOrEmpty(line))
            {
                SegmentLine(line);
            }
        }

        /// <summary>Segments next line from stream.</summary>
        private async Task SegmentNextLineAsync()
        {
            Assert(_stream != null);

            var line = await ReadNextLineAsync();

            if (!IsNullOrEmpty(line))
            {
                SegmentLine(line);
            }
        }

        /// <summary>Segments next line from stream.</summary>
        private void SegmentLine(String line)
        {
            Assert(!IsNullOrEmpty(line));
            Assert(_delimiters != null);
            Assert(_delimiters.Any());
            Assert(_segments != null);

            // split the line to terms
            var segments = line.Split(_delimiters).Where(term => !IsNullOrWhiteSpace(term));

            // queue segmented terms
            var column = 1;
            foreach (var segment in segments)
            {
                _segments.Enqueue(new Segment(segment, _lineNumber, column));
                column += segment.Length;
            }
        }

        /// <summary>Reads next line from string while it is empty.</summary>
        private String ReadNextLine()
        {
            Assert(_stream != null);

            String line;

            do
            {
                // this loop allows to skip empty lines
                line = _stream.ReadLine();
                _lineNumber++;
            } while (!_stream.EndOfStream && IsNullOrEmpty(line));

            return line;
        }

        /// <summary>Reads next line from string while it is empty.</summary>
        private async Task<String> ReadNextLineAsync()
        {
            Assert(_stream != null);

            String line;

            do
            {
                // this loop allows to skip empty lines
                line = await _stream.ReadLineAsync();
                _lineNumber++;
            } while (!_stream.EndOfStream && IsNullOrEmpty(line));

            return line;
        }
    }
}
