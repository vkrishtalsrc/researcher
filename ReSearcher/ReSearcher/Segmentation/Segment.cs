﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Diagnostics;
using static System.Diagnostics.Debug;

namespace ReSearcher.Segmentation
{
    /// <summary>Represents text segment from a file.</summary>
    [DebuggerDisplay("{" + nameof(Value) + "}")]
    internal class Segment
    {
        /// <summary>Creates a new instance of <see cref="Segment" />.</summary>
        /// <param name="value">The segment value. Can be empty but can't be null.</param>
        /// <param name="lineNumber">The line number of the segment if file.</param>
        /// <param name="positionOnLine">The position of this segment on the line.</param>
        public Segment(String value, Int32 lineNumber, Int32 positionOnLine)
        {
            Assert(!String.IsNullOrEmpty(value));
            Assert(lineNumber > 0);
            Assert(positionOnLine > 0);

            Value = value;
            Position = new FilePosition(lineNumber, positionOnLine);
        }

        /// <summary>The segment value.</summary>
        public String Value { get; }

        /// <summary>Token positions in file.</summary>
        public FilePosition Position { get; }
    }
}
