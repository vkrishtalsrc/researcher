﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reactive.Linq;
using ReSearcher.Segmentation;
using static System.String;

namespace ReSearcher.Indexes.TunableIndexes
{
    /// <summary>
    /// Represents fully tunable inverted index based on hash table data structure.
    /// HashTable is a data structure which implements an associative array abstract data type, a structure that can map
    /// keys to values.
    /// A hash table uses a hash function to compute an index into an array of buckets or slots, from which the desired
    /// value can be found.
    /// </summary>
    /// ry>
    internal sealed class HashTable : IIndex
    {
        /// <summary>
        /// The index data - thread-safe hash table where:
        /// Key - segment value;
        /// Value - thread-safe hashtable with file identifiers + file positions.
        /// </summary>
        private readonly ConcurrentDictionary<String, ConcurrentDictionary<String, ConcurrentBag<FilePosition>>> _data =
            new ConcurrentDictionary<String, ConcurrentDictionary<String, ConcurrentBag<FilePosition>>>();

        /// <summary>Adds file segment into the index.</summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="segment">The file segment.</param>
        public void Add(String fileName, Segment segment)
        {
            Debug.Assert(_data != null);
            Debug.Assert(!IsNullOrEmpty(fileName));
            Debug.Assert(segment != null);
            Debug.Assert(segment.Value != null);

            // create new map for segments if it is not exists
            // create new bag with file positions if it is not exists
            // add segment file position into bag
            _data.GetOrAdd(segment.Value, new ConcurrentDictionary<String, ConcurrentBag<FilePosition>>())
                 .GetOrAdd(fileName, new ConcurrentBag<FilePosition>())
                 .Add(segment.Position);
        }

        /// <summary>Searches text in the index.</summary>
        /// <param name="text">The text to search.</param>
        /// <returns></returns>
        public IObservable<SearchResult> Search(String text)
        {
            Debug.Assert(_data != null);
            Debug.Assert(!IsNullOrEmpty(text));

            if (_data.TryGetValue(text, out var files))
            {
                return files.ToObservable()
                            .Select(pair => new SearchResult(pair.Key, pair.Value));
            }

            return Observable.Empty<SearchResult>();
        }

        /// <summary>Clears index.</summary>
        public void Clear()
        {
            _data?.Clear();
        }
    }
}
