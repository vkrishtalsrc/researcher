﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using ReSearcher.Segmentation;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher.Indexes.TunableIndexes
{
    /// <summary>
    /// Represents fully tunable inverted index based on Prefix Tree data structure.
    /// Prefix tree is a kind of search tree—an ordered tree data structure
    /// that is used to store a dynamic set or associative array where the keys are usually strings.
    /// Unlike a binary search tree, no node in the tree stores the key associated with that node;
    /// instead, its position in the tree defines the key with which it is associated.
    /// </summary>
    internal sealed class PrefixTree : IIndex
    {
        /// <summary>The thread-sage map with node data.</summary>
        private readonly Lazy<ConcurrentDictionary<String, ConcurrentBag<FilePosition>>> _data =
            new Lazy<ConcurrentDictionary<String, ConcurrentBag<FilePosition>>>();

        /// <summary>The thread-safe map with child nodes.</summary>
        private readonly Lazy<ConcurrentDictionary<Char, PrefixTree>> _subtrees =
            new Lazy<ConcurrentDictionary<Char, PrefixTree>>();

        /// <summary>Adds file segment into the index.</summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="segment">The file segment.</param>
        public void Add(String fileName, Segment segment)
        {
            Assert(!IsNullOrEmpty(fileName));
            Assert(segment != null);
            Assert(!IsNullOrEmpty(segment.Value));

            Add(segment, fileName, 0);
        }

        /// <summary>Searches text inside the index.</summary>
        /// <param name="text">The text to search.</param>
        /// <returns></returns>
        public IObservable<SearchResult> Search(String text)
        {
            Assert(!IsNullOrEmpty(text));

            return Get(text, 0).ToObservable();
        }

        /// <summary>Clears index.</summary>
        public void Clear()
        {
            _data?.Value.Clear();
            _subtrees?.Value.Clear();
        }

        /// <summary>Adds file segment into the index.</summary>
        /// <param name="segment">The file segment.</param>
        /// <param name="fileName">The file name.</param>
        /// <param name="index">The index of current symbol from the segment value.</param>
        private void Add(Segment segment, String fileName, Int32 index)
        {
            Assert(segment != null);
            Assert(!IsNullOrEmpty(segment.Value));
            Assert(index >= 0);
            Assert(index <= segment.Value.Length);
            Assert(_data != null);
            Assert(_subtrees != null);

            if (index == segment.Value.Length)
            {
                // initialize a lazy dictionary
                // create new bag if it is not exists
                // add the position into existing bag
                _data.Value
                     .GetOrAdd(fileName, new ConcurrentBag<FilePosition>())
                     .Add(segment.Position);
            }
            else
            {
                // initialize a lazy dictionary
                // create new subtree if it is not exists
                // add a segment tail into the subtree 
                _subtrees.Value
                         .GetOrAdd(segment.Value[index], new PrefixTree())
                         .Add(segment, fileName, ++index);
            }
        }

        /// <summary>Gets values associated with the specified key.</summary>
        /// <param name="key">The key of the value to get.</param>
        /// <param name="index">The start index of item from the key.</param>
        /// <returns>
        ///     Values associated with the specified key.
        ///     If the specified key does not exist in the prefix tree, a get operation returns an empty
        ///     <see cref="IEnumerable{T}" />.
        /// </returns>
        private IEnumerable<SearchResult> Get(String key, Int32 index)
        {
            Assert(!IsNullOrEmpty(key));
            Assert(index >= 0);
            Assert(index <= key.Length);
            Assert(_data != null);
            Assert(_subtrees != null);

            if (index == key.Length)
            {
                // because of the index is the end of the key
                // we can return values of current node
                return _data.IsValueCreated
                    ? GetCurrentData()
                    : Enumerable.Empty<SearchResult>();
            }

            // search the next symbol deeper inside tree
            return _subtrees.Value.TryGetValue(key[index], out var subtree)
                ? subtree.Get(key, ++index)
                : Enumerable.Empty<SearchResult>();
        }

        /// <summary>Returns index data from current subtree.</summary>
        /// <returns>The search result.</returns>
        private IEnumerable<SearchResult> GetCurrentData()
        {
            Assert(_data != null);
            Assert(_data.IsValueCreated);

            foreach (var value in _data.Value)
            {
                yield return new SearchResult(value.Key, value.Value);
            }
        }
    }
}
