﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ReSearcher.Exceptions;
using ReSearcher.Segmentation;
using static System.Diagnostics.Debug;

namespace ReSearcher.Indexes
{
    /// <summary>Represents builder that can update indexes by data from files.</summary>
    /// <typeparam name="T">The type of Segmentator.</typeparam>
    internal class PlainTextIndexBuilder<T> : IIndexBuilder where T : IFileSegmentator, new()
    {
        /// <summary>The file segmentator.</summary>
        private readonly IFileSegmentator _fileSegmentator = new T();

        /// <summary>Adds a file into the index.</summary>
        /// <param name="index">The source index to modifying.</param>
        /// <param name="path">The file name.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="index" /> is null.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="FileNotFoundException">The file to index was not found.</exception>
        public async Task IndexFileAsync(IIndex index, String path)
        {
            Assert(_fileSegmentator != null);

            if (index == null)
            {
                throw new ArgumentNullException(nameof(index));
            }

            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("File to index was not found.", path);
            }

            using (var stream = _fileSegmentator.StartSegmentation(path))
            {
                var segment = await stream.NextSegmentAsync();

                while (segment != null)
                {
                    index.Add(path, segment);
                    segment = await stream.NextSegmentAsync();
                }
            }
        }

        /// <summary>Adds files from directory into the index.</summary>
        /// <param name="index">The source index to modifying.</param>
        /// <param name="path">The relative or absolute path to the directory to search. This string is not case-sensitive.</param>
        /// <param name="filter">
        ///     The filter to match against the names of files in directory.
        ///     This parameter can contain a combination of valid literal path and wildcard (* and ?) characters,
        ///     but it doesn't support regular expressions.
        /// </param>
        /// <param name="option">
        ///     One of the enumeration values that specifies whether the search operation
        ///     should include all subdirectories or only the current directory.
        /// </param>
        /// <exception cref="ArgumentNullException">The <paramref name="index" /> is null.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="DirectoryNotFoundException">The directory was not found.</exception>
        public Task IndexFilesAsync(IIndex index, String path, String filter, SearchOption option)
        {
            Assert(_fileSegmentator != null);

            if (index == null)
            {
                throw new ArgumentNullException(nameof(index));
            }

            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException();
            }

            // get files from directory by filter
            var files = Directory.GetFiles(path, filter, option);

            if (files.Any())
            {
                // start indexing of all files together
                return Task.WhenAll(files.Select(file => IndexFileAsync(index, file)));
            }

            // there are no files in directory
            return Task.CompletedTask;
        }

        /// <summary>Adds a file into the index.</summary>
        /// <param name="index">The source index to modifying.</param>
        /// <param name="path">The file name.</param>
        /// <exception cref="ArgumentNullException">The <paramref name="index" /> is null.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="FileNotFoundException">The file to index was not found.</exception>
        public void IndexFile(IIndex index, String path)
        {
            Assert(_fileSegmentator != null);

            if (index == null)
            {
                throw new ArgumentNullException(nameof(index));
            }

            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("File to index was not found.", path);
            }

            using (var stream = _fileSegmentator.StartSegmentation(path))
            {
                var segment = stream.NextSegment();

                while (segment != null)
                {
                    index.Add(path, segment);
                    segment = stream.NextSegment();
                }
            }
        }

        /// <summary>Adds files from directory into the index.</summary>
        /// <param name="index">The source index to modifying.</param>
        /// <param name="path">The relative or absolute path to the directory to search. This string is not case-sensitive.</param>
        /// <param name="filter">
        ///     The filter to match against the names of files in directory.
        ///     This parameter can contain a combination of valid literal path and wildcard (* and ?) characters,
        ///     but it doesn't support regular expressions.
        /// </param>
        /// <param name="option">
        ///     One of the enumeration values that specifies whether the search operation
        ///     should include all subdirectories or only the current directory.
        /// </param>
        /// <exception cref="ArgumentNullException">The <paramref name="index" /> is null.</exception>
        /// <exception cref="ArgumentNullOrEmptyException">The <paramref name="path" /> is null or empty.</exception>
        /// <exception cref="DirectoryNotFoundException">The directory was not found.</exception>
        public void IndexFiles(IIndex index, String path, String filter, SearchOption option)
        {
            Assert(_fileSegmentator != null);

            if (index == null)
            {
                throw new ArgumentNullException(nameof(index));
            }

            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullOrEmptyException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException();
            }

            // get files from directory by filter
            var files = Directory.GetFiles(path, filter, option);

            foreach (var file in files)
            {
                IndexFile(index, file);
            }
        }
    }
}
