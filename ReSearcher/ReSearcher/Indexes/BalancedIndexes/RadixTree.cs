﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using ReSearcher.Segmentation;

namespace ReSearcher.Indexes.BalancedIndexes
{
    internal sealed class RadixTree : IBalancedIndex
    {
        /// <summary>Adds file segment into the index.</summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="segment">The file segment.</param>
        public void Add(String fileName, Segment segment)
        {
            throw new NotImplementedException();
        }

        /// <summary>Searches text in the index.</summary>
        /// <param name="text">The text to search.</param>
        /// <returns></returns>
        public IObservable<SearchResult> Search(String text)
        {
            throw new NotImplementedException();
        }

        /// <summary>Clears index.</summary>
        public void Clear()
        {
            throw new NotImplementedException();
        }

        /// <summary>Clears data just for specific file.</summary>
        /// <param name="fileName">The file name.</param>
        public void ClearFor(String fileName)
        {
            throw new NotImplementedException();
        }
    }
}
