﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reactive.Linq;
using ReSearcher.Segmentation;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher.Indexes.BalancedIndexes
{
    /// <summary>Represents balanced (multi-index) inverted index based on hash table data structure.</summary>
    internal class HashTable : IBalancedIndex
    {
        /// <summary>
        ///     The index data - thread-safe hash table:
        ///     Key - file name;
        ///     Value - thread-safe hashtable with segment values + file positions of these segments.
        /// </summary>
        private readonly
            ConcurrentDictionary<String, ConcurrentDictionary<String, ConcurrentBag<FilePosition>>> _data =
                new ConcurrentDictionary<String, ConcurrentDictionary<String, ConcurrentBag<FilePosition>>>();

        /// <summary>Adds file segment into the index.</summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="segment">The file segment.</param>
        public void Add(String fileName, Segment segment)
        {
            Assert(_data != null);
            Assert(!IsNullOrEmpty(fileName));
            Assert(segment != null);
            Assert(segment.Value != null);

            _data.GetOrAdd(fileName, new ConcurrentDictionary<String, ConcurrentBag<FilePosition>>())
                 .GetOrAdd(segment.Value, new ConcurrentBag<FilePosition>())
                 .Add(segment.Position);
        }

        /// <summary>Searches text in the index.</summary>
        /// <param name="text">The text to search.</param>
        /// <returns></returns>
        public IObservable<SearchResult> Search(String text)
        {
            Assert(_data != null);
            Assert(!IsNullOrEmpty(text));

            if (_data.Any())
            {
                return Observable
                    .For(_data, fileIndex =>
                    {
                        if (fileIndex.Value.TryGetValue(text, out var positions))
                        {
                            return Observable.Return(new SearchResult(fileIndex.Key, positions));
                        }

                        return Observable.Empty<SearchResult>();
                    });
            }

            return Observable.Empty<SearchResult>();
        }

        /// <summary>Clears index.</summary>
        public void Clear()
        {
            Assert(_data != null);
            _data.Clear();
        }

        /// <summary>Clears data just for specific file.</summary>
        /// <param name="fileName">The file name.</param>
        public void ClearFor(String fileName)
        {
            Assert(_data != null);

            if (!_data.TryRemove(fileName, out var index) && _data.ContainsKey(fileName))
            {
                index?.Clear();
            }
        }
    }
}
