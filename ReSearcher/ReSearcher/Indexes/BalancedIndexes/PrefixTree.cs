﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using ReSearcher.Segmentation;
using static System.Diagnostics.Debug;
using static System.String;

namespace ReSearcher.Indexes.BalancedIndexes
{
    /// <summary>
    ///     Represents balanced (multi-index) inverted index based on prefix tree data structure.
    ///     Prefix tree is a kind of search tree—an ordered tree data structure
    ///     that is used to store a dynamic set or associative array where the keys are usually strings.
    ///     Unlike a binary search tree, no node in the tree stores the key associated with that node;
    ///     instead, its position in the tree defines the key with which it is associated.
    /// </summary>
    internal sealed class PrefixTree : IBalancedIndex
    {
        /// <summary>The separated indexes for each file.</summary>
        private readonly Lazy<ConcurrentDictionary<String, Tree>> _indexes =
            new Lazy<ConcurrentDictionary<String, Tree>>();

        /// <summary>Adds file segment into the index.</summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="segment">The file segment.</param>
        public void Add(String fileName, Segment segment)
        {
            Assert(!IsNullOrEmpty(fileName));
            Assert(segment != null);
            Assert(!IsNullOrEmpty(segment.Value));
            Assert(_indexes != null);

            _indexes
                .Value
                .GetOrAdd(fileName, new Tree())
                .Add(segment, 0);
        }

        /// <summary>Searches text inside the index.</summary>
        /// <param name="text">The text to search.</param>
        /// <returns></returns>
        public IObservable<SearchResult> Search(String text)
        {
            Assert(!IsNullOrEmpty(text));
            Assert(_indexes != null);

            if (_indexes.IsValueCreated && _indexes.Value.Any())
            {
                return Observable
                    .For(_indexes.Value, index =>
                    {
                        var positions = index.Value.Get(text, 0);

                        Assert(positions != null);

                        if (positions.Any())
                        {
                            return Observable.Return(new SearchResult(index.Key, positions));
                        }

                        return Observable.Empty<SearchResult>();
                    });
            }

            return Observable.Empty<SearchResult>();
        }

        /// <summary>Clears index.</summary>
        public void Clear()
        {
            Assert(_indexes != null);
            _indexes.Value.Clear();
        }

        /// <summary>Clears data just for specific file.</summary>
        /// <param name="fileName">The file name.</param>
        public void ClearFor(String fileName)
        {
            Assert(_indexes != null);

            _indexes.Value.TryRemove(fileName, out var index);
        }

        /// <summary>
        ///     Represents index based on prefix tree data structure.
        ///     Leafs of the tree contain file positions only without file name.
        /// </summary>
        private class Tree
        {
            /// <summary>The thread-sage map with node data.</summary>
            private readonly Lazy<ConcurrentBag<FilePosition>> _data =
                new Lazy<ConcurrentBag<FilePosition>>();

            /// <summary>The thread-safe map with child nodes.</summary>
            private readonly Lazy<ConcurrentDictionary<Char, Tree>> _subtrees =
                new Lazy<ConcurrentDictionary<Char, Tree>>();

            /// <summary>Adds file segment into the index.</summary>
            /// <param name="segment">The file segment.</param>
            /// <param name="index">The index of current symbol from the segment value.</param>
            public void Add(Segment segment, Int32 index)
            {
                Assert(segment != null);
                Assert(!IsNullOrEmpty(segment.Value));
                Assert(index >= 0);
                Assert(index <= segment.Value.Length);
                Assert(_data != null);
                Assert(_subtrees != null);

                if (index == segment.Value.Length)
                {
                    _data.Value.Add(segment.Position);
                }
                else
                {
                    // initialize a lazy dictionary
                    // create new subtree if it is not exists
                    // add a segment tail into the subtree 
                    _subtrees.Value
                             .GetOrAdd(segment.Value[index], new Tree())
                             .Add(segment, ++index);
                }
            }

            /// <summary>Gets values associated with the specified key.</summary>
            /// <param name="key">The key of the value to get.</param>
            /// <param name="index">The start index of item from the key.</param>
            /// <returns>
            ///     Values associated with the specified key.
            ///     If the specified key does not exist in the prefix tree, a get operation returns an empty
            ///     <see cref="IEnumerable{T}" />.
            /// </returns>
            public IEnumerable<FilePosition> Get(String key, Int32 index)
            {
                Assert(!IsNullOrEmpty(key));
                Assert(index >= 0);
                Assert(index <= key.Length);
                Assert(_data != null);
                Assert(_subtrees != null);

                if (index == key.Length)
                {
                    return _data.IsValueCreated
                        ? _data.Value
                        : Enumerable.Empty<FilePosition>();
                }

                return _subtrees.Value.TryGetValue(key[index], out var subtree)
                    ? subtree.Get(key, ++index)
                    : Enumerable.Empty<FilePosition>();
            }
        }
    }
}
