﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Reactive.Linq;
using static System.Diagnostics.Debug;

namespace ReSearcher.Extensions
{
    /// <summary>Extension methods for <see cref="Observable" />.</summary>
    internal static class ObservableExtensions
    {
        /// <summary>Filters sequence and skips elements that equals null.</summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="self">The reference to Observable.</param>
        public static IObservable<T> WhereNotNull<T>(this IObservable<T> self) where T : class
        {
            Assert(self != null);
            return self.Where(value => value != null);
        }

        /// <summary>Filters sequence and skips elements that equals null.</summary>
        /// <typeparam name="T">The element type.</typeparam>
        /// <param name="self">The reference to Observable.</param>
        public static IObservable<T?> WhereNotNull<T>(this IObservable<T?> self) where T : struct
        {
            Assert(self != null);
            return self.Where(value => value.HasValue);
        }
    }
}
