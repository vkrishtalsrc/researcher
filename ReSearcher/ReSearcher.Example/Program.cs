﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Drawing;
using EntryPoint;
using Console = Colorful.Console;

namespace ReSearcher.Example
{
    /// <summary>The program entry point.</summary>
    internal static class Program
    {
        /// <summary>The application name.</summary>
        public const String PROGRAM_NAME = "ReSearcher.Example";

        /// <summary>The ReSearcher instance the example.</summary>
        public static ReSearcher Researcher { get; set; } =
            new ReSearcher(IndexType.HashTable, IndexStrategy.FullyTunableIndex);

        /// <summary>Subscription that need to dispose each time when something was searched.</summary>
        public static IDisposable SearchSubscription { get; set; }

        /// <summary>The program entry point method.</summary>
        /// <param name="arguments">The console line arguments. Not used.</param>
        private static void Main(String[] arguments)
        {
            // render logo
            Console.WriteAscii("ReSearcher", Color.LawnGreen);

            while (true)
            {
                // ask a command
                Console.Write($"{Environment.NewLine}Type a command: ", Color.LawnGreen);
                var command = Console.ReadLine().Split();

                // execute the command
                try
                {
                    Cli.Execute<Commands>(command);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"[{DateTime.Now:T}]: {e.Message}", Color.Red);
                }
            }
        }
    }
}
