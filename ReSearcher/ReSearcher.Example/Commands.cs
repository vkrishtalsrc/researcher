﻿// Copyright (c) 2018
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

using System;
using System.Drawing;
using System.IO;
using System.Linq;
using EntryPoint;
using EntryPoint.Exceptions;
using static System.Diagnostics.Debug;
using static ReSearcher.Example.Program;
using Console = Colorful.Console;

namespace ReSearcher.Example
{
    internal sealed class Commands : BaseCliCommands
    {
        [Command("new-index")]
        [Help("Creates empty index.")]
        public void CreateIndex(String[] arguments)
        {
            var args = Cli.Parse<NewIndexArguments>(arguments);

            if (args.UserFacingExceptionThrown)
            {
                return;
            }

            SearchSubscription?.Dispose();
            Researcher?.Dispose();
            Researcher = new ReSearcher(IndexType.PrefixTree, IndexStrategy.BalancedMultiIndex);
            Console.WriteLine($"[{DateTime.Now:T}]: New index was created.", Color.LawnGreen);
        }

        [Command("clear")]
        [Help("Clears the screen.")]
        public void Clear(String[] arguments)
        {
            Console.Clear();
            Console.WriteAscii("ReSearcher", Color.LawnGreen);
        }

        [Command("exit")]
        [Help("Closes the current apps.")]
        public void Exit(String[] arguments)
        {
            SearchSubscription?.Dispose();
            Researcher?.Dispose();

            Environment.Exit(0);
        }

        [Command("index-file")]
        [Help("Adds a file into the index.")]
        public void IndexFile(String[] arguments)
        {
            Assert(Researcher != null);

            var args = Cli.Parse<IndexFileArguments>(arguments);

            if (args.UserFacingExceptionThrown)
            {
                return;
            }

            Researcher.ResearchFile(args.Path);

            Console.WriteLine($"[{DateTime.Now:T}]: Index was built.", Color.LawnGreen);
        }

        [Command("index-directory")]
        [Help("Adds a directory into the index.")]
        public void IndexDirectory(String[] arguments)
        {
            Assert(Researcher != null);

            var args = Cli.Parse<IndexDirectoryArguments>(arguments);

            if (args.UserFacingExceptionThrown)
            {
                return;
            }

            Researcher.ResearchDirectory(args.Path, args.Filter,
                args.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            Console.WriteLine($"[{DateTime.Now:T}]: Index was built.", Color.LawnGreen);
        }

        [Command("search")]
        [Help("Searches word inside the index.")]
        public void IndexSearch(String[] arguments)
        {
            Assert(Researcher != null);

            var args = Cli.Parse<SearchArguments>(arguments);

            if (args.UserFacingExceptionThrown)
            {
                return;
            }

            // dispose subscription to previous search
            SearchSubscription?.Dispose();

            // subscribe to new search
            SearchSubscription = Researcher
                                 .Search(args.Word)
                                 .Subscribe(DisplaySearchResult);
        }

        /// <summary>Displays help info.</summary>
        public override void OnHelpInvoked(String helpText)
        {
            Console.WriteLine(helpText, Color.Yellow);
        }

        /// <summary>Displays error message.</summary>
        public override void OnUserFacingException(UserFacingException e, String message)
        {
            // display the error
            Console.WriteLine($"[{DateTime.Now:T}]: {message}", Color.Red);

            // display the help
            var help = Cli.GetHelp<Commands>();
            Console.WriteLine(help, Color.Yellow);
        }

        /// <summary>Displays a search result.</summary>
        private void DisplaySearchResult(SearchResult result)
        {
            Assert(result != null);

            // expected format is {[line:column], [line:column], [line:column], ....}
            var positions = result.Positions
                                  .Select(pos => $"[{pos.Line}:{pos.Column}]")
                                  .Aggregate((all, current) => $"{all}, {current}");

            Console.WriteLine($"[{DateTime.Now:T}]: {result.FileName} - {{{positions}}}", Color.LawnGreen);
        }
    }
}
