# ReSearcher #

ReSearcher is a library that can create in-memory indexes of text files and rebuild indexes automatically after some file changed.

## Supported index types ##

* Prefix tree - kind of search tree to store a dynamic set or associative array.Unlike a binary search tree, no node in the tree stores the key associated with that node;instead, its position in the tree defines the key with which it is associated.All the descendants of a node have a common prefix of the string associated with that node,and the root is associated with the empty string.Values are not necessarily associated with every node.Rather, values tend only to be associated with leaves, and with some inner nodes that correspond to keys ofinterest.This index type is appropriate candidate to quick search if it is requred to index a lot of data without economy of memory.
* Hash table - uses a hash function to compute an index into an array of buckets or slots, from which the desiredvalue can be found.It means that some collisions are possible. Collisions can degrade perfomance at whole.This index type is appropriate candidate to quick search if it isn't requred to index a huge data.
* Radix tree [NOT IMPLMENETED] - the space-optimized prefix tree in which each node that is the only child is merged with its parent.This index type is appropriate candidate to quick search if it is requred to index a lot of data with economy ofmemoryand longer index rebuild is acceptable.

## Index strategies ##
* Balanced index - appropriate candidate if updates required more often then search
* Fully Tunable index - appropriate candidate if search required more often then updates


## Using ##

1. Create instance of ReSearcher class. By default it uses balanced prefix tree but you can specify other index type via ReSearcher.IndexType and ReSearcher.IndexStrategy.
2. Add file or directory to index via methods ```ResearchFile(...)``` \ ```ResearchDirectory(...)``` \ ```ResearchFileAsync(...)``` \ ```ResearchDirectoryAsync(...)```.
3. Try to search required token and subscribe to result from ```IObservalbe<SearchResult>``` via method ```Search(String text)``` of your researcher instance.
4. Dispose researcher to free resources.

## Examples ##
____
```
// a way to index a directory via fully tunable inverted index based on prefix tree:
var researcher = new ReSearcher(IndexType.PrefixTree, IndexStrategy.FullyTunableIndex);
researcher.ResearchDirectory(@"path\to\directory", "*.cs", SearchOption.AllDirectories);
var subscription = Researcher.Search("word").Subscribe(result => {/* hanlde the search result */});
```
____

```
// a way to index a directory via balanced inverted index based on hash table:
var researcher = new ReSearcher(IndexType.HashTable, IndexStrategy.BalancedMultiIndex);
researcher.ResearchDirectory(@"path\to\directory", "*.cs", SearchOption.AllDirectories);
var subscription = Researcher.Search("someword").Subscribe(result => {/* hanlde the search result */});
```